<?php

namespace Drupal\date_all_day\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;

/**
 * Plugin implementation of the 'daterange' field type.
 *
 * @FieldType(
 *   id = "daterange_all_day",
 *   label = @Translation("Date time range (All day) (DEPRECATED)"),
 *   description = @Translation("This field type is deprecated. Use daterande instead."),
 *   default_widget = "daterange_all_day",
 *   default_formatter = "daterange_all_day_default",
 *   list_class = "\Drupal\datetime_range\Plugin\Field\FieldType\DateRangeFieldItemList"
 * )
 */
class DatetimeRangeAllDayItem extends DateRangeItem { }
